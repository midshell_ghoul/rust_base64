This is a simplistic Base64 implementation written in Rust, which implements encode and decode using a custom alphabet or default alphabet (as defined in RFC 4648).

This package is developed for learning purposes only, and should not be used instead of the official crate (found here: https://crates.io/crates/base64)