pub mod my_base64;

extern crate base64;
use std::fs;
use std::io;

struct TestResults {
    succeeded: u32,
    failed: u32,
    total_num: u32,
}

impl TestResults {
    pub fn succeeded(self: &mut Self) {
        self.succeeded += 1;
        self.total_num += 1;
    }
    
    pub fn failed(self: &mut Self) {
        self.failed += 1;
        self.total_num += 1;
    }

    pub fn add(self: &mut Self, secondary_results_object: & TestResults) {
        self.succeeded += secondary_results_object.succeeded;
        self.failed += secondary_results_object.failed;
        self.total_num += secondary_results_object.total_num;
    }
}

fn test_encoding(file_data: & Vec<u8>) -> Result<(), ()> {
    let b64 = my_base64::MyBase64::new(None, None);
    let my_b64_encode = b64.encode(&file_data);
    let official_b64_encode = base64::encode(&file_data);

    if my_b64_encode == official_b64_encode { 
        Ok(())
    } else {
        Err(())
    }
}

fn test_decoding(file_data: & Vec<u8>) -> Result<(), ()> {
    let official_b64_encode = base64::encode(&file_data);

    let b64 = my_base64::MyBase64::new(None, None);
    let my_b64_decode = b64.decode(&official_b64_encode)
                                .expect("Failed to decode base64 string");
    let official_b64_decode = base64::decode(&official_b64_encode)
                                            .expect("Failed to decode base64 string");

    if my_b64_decode == official_b64_decode { 
        Ok(())
    } else {
        Err(())
    }
}

fn test_base64_implementation(file: io::Result<fs::DirEntry>) -> TestResults {
    let mut test_result = TestResults{ succeeded: 0, failed: 0, total_num: 0 };

    let file = file.expect("Failed to collect file");
    println!("Testing {}", file.path()
                               .display());

    let file_data = fs::read(file.path())
                            .expect("Failed to read file");

    match test_encoding(&file_data) {
        Ok(())  => { test_result.succeeded(); },
        Err(()) => { test_result.failed(); },
    }

    match test_decoding(&file_data) {
        Ok(())  => { test_result.succeeded(); },
        Err(()) => { test_result.failed(); },
    }

    test_result
}

fn main() {
    let mut test_results: TestResults;
    let mut total_results = TestResults{ succeeded: 0, failed: 0, total_num: 0 };

    let testcases_dir = fs::read_dir("testcases")
                                    .expect("Failed to read_dir");
    
    for file in testcases_dir {
        test_results = test_base64_implementation(file);
        total_results.add(&test_results);
    }

    println!("Tested {} cases", total_results.total_num);
    println!("\t{} succeeeded and {} failed", total_results.succeeded, total_results.failed);
}
