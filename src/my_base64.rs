pub struct MyBase64<'a> {
    pub alphabet: &'a [u8],
    pub padding: char,
}

impl MyBase64<'_> {
    pub fn new(alphabet: Option<&'static str>, padding: Option<char>) -> MyBase64<'static> {
        // TODO: Validate the Optional alphabet and padding (length, unique characters, printable, etc)
        let default_alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
                                    .as_bytes();
        let default_padding = '=';

        let alphabet_used = match alphabet {
            Some(custom_alpha) => custom_alpha.as_bytes(),
            None => default_alphabet,
        };

        let padding_used = match padding {
            Some(custom_padding) => custom_padding,
            None => default_padding,
        };

        MyBase64{alphabet: alphabet_used, padding: padding_used}
    }
    
    pub fn encode(self: &Self, binary_data: &Vec<u8>) -> String {
        // We know that the resulting base64 string with be 4/3rds the size of the original data.
        // Pre-allocate this to increase performance in the case that the data is particularly large.
        let mut encoded_string = String::with_capacity((binary_data.len() as f32 * (4.0/3.0)) as usize);

        let binary_data_len = binary_data.len();
        let mut index = 0;
        let mut octet: char;
        let mut char_one: u8;
        let mut char_two: u8;
        let mut char_three: u8;

        while index < binary_data_len {
            match binary_data_len - index {
                len if len >= 3 => {
                    char_one = binary_data[index];
                    char_two = binary_data[index+1];
                    char_three = binary_data[index+2];

                    octet = self.encode_first_octet(char_one);
                    encoded_string.push(octet);

                    octet = self.encode_second_octet(char_one, Some(char_two));
                    encoded_string.push(octet);

                    octet = self.encode_third_octet(char_two, Some(char_three));
                    encoded_string.push(octet);

                    octet = self.encode_fourth_octet(char_three);
                    encoded_string.push(octet);

                    index += 3;
                }
                2 => {
                    char_one = binary_data[index];
                    char_two = binary_data[index+1];
                    
                    octet = self.encode_first_octet(char_one);
                    encoded_string.push(octet);

                    octet = self.encode_second_octet(char_one, Some(char_two));
                    encoded_string.push(octet);
                    
                    octet = self.encode_third_octet(char_two, None);
                    encoded_string.push(octet);  

                    encoded_string.push(self.padding);  
                    
                    index += 2;
                }
                1 => {
                    char_one = binary_data[index];

                    octet = self.encode_first_octet(char_one);
                    encoded_string.push(octet);

                    octet = self.encode_second_octet(char_one, None);
                    encoded_string.push(octet);
                    
                    encoded_string.push(self.padding);  

                    encoded_string.push(self.padding);  
                    
                    index += 1;
                }
                _ => {
                    panic!("Que? remaining_len is {}, but we are still in the loop",
                            binary_data_len - index);
                }
            };
        }

        encoded_string
    }

    fn encode_first_octet(self: &Self, first_byte: u8) -> char {
        let character_index: usize = (first_byte >> 2) as usize;
        self.alphabet[character_index] as char 
    }
    
    fn encode_second_octet(self: &Self, first_byte: u8, second_byte: Option<u8>) -> char {
        let character_index: usize = match second_byte {
            Some(s_byte) => ((first_byte << 6 >> 2) | (s_byte >> 4)) as usize,
            None => (first_byte << 6 >> 2) as usize,
        };
        
        self.alphabet[character_index] as char  
    }
    
    fn encode_third_octet(self: &Self, second_byte: u8, third_byte: Option<u8>) -> char {
        let character_index: usize = match third_byte {
            Some(t_byte) => ((second_byte << 4 >> 2) | (t_byte >> 6)) as usize,
            None => (second_byte << 4 >> 2) as usize,
        };
        
        self.alphabet[character_index] as char
    }
    
    fn encode_fourth_octet(self: &Self, third_byte: u8) -> char {
        let character_index: usize = (third_byte & 0x3F) as usize;
        
        self.alphabet[character_index] as char
    }

    pub fn decode(self: &Self, b64_data: &String) -> Result<Vec<u8>, ()> {
        let mut decoded_bytes = Vec::<u8>::new();

        // Cloning b64_data to get a mutable reference and remove = padding. This solution has memory usage 
        // of 3 * size of the base64 encoded string (base64 encoded string, mutable clone of base64 encoded
        // string, and the resulting decoded data). There is probably a better solution that only uses twice 
        // the size of the base64 encoded data, whilst still keeping the parameter as a &String, rather than
        // &mut String. I don't know that solution yet, but it seems to be what the official base64 encoder 
        // does. TODO
        let mut b64_data = b64_data.clone();
        b64_data.retain(|c| c != '=');
        
        let b64_data = b64_data.as_bytes();
        let b64_data_len = b64_data.len();
        let mut index = 0;
        let mut char_one: u8;
        let mut char_two: u8;
        let mut char_three: u8;
        let mut char_four: u8;
        let mut char_one_index: u8;
        let mut char_two_index: u8;
        let mut char_three_index: u8;
        let mut char_four_index: u8;

        while index < b64_data_len {
            match b64_data_len - index {
                len if len >= 4 => {
                    // Grab 4 b64 encoded characters at a time
                    char_one =   b64_data[index];
                    char_two =   b64_data[index+1];
                    char_three = b64_data[index+2];
                    char_four =  b64_data[index+3];

                    // TODO: Change these expects to a form of unwrap that either passes through Some(), 
                    // or returns an Err() out of the function if None
                    char_one_index = self.get_index_in_alpha(&char_one)
                                                    .expect(format!("Char {} not found in alphabet", char_one as char)
                                                                .as_str());
                    char_two_index = self.get_index_in_alpha(&char_two)
                                                    .expect(format!("Char {} not found in alphabet", char_two as char)
                                                                .as_str());
                    char_three_index = self.get_index_in_alpha(&char_three)
                                                    .expect(format!("Char {} not found in alphabet", char_three as char)
                                                                .as_str());
                    char_four_index = self.get_index_in_alpha(&char_four)
                                                    .expect(format!("Char {} not found in alphabet", char_four as char)
                                                                .as_str());


                    decoded_bytes.push(char_one_index << 2 | char_two_index >> 4);
                    decoded_bytes.push(char_two_index << 4 | char_three_index >> 2);
                    decoded_bytes.push(char_three_index << 6 | char_four_index & 0x3f);

                    index += 4;
                }
                3 => {
                    char_one =   b64_data[index];
                    char_two =   b64_data[index+1];
                    char_three = b64_data[index+2];

                    char_one_index = self.get_index_in_alpha(&char_one)
                                                    .expect(format!("Char {} not found in alphabet", char_one as char)
                                                                .as_str());
                    char_two_index = self.get_index_in_alpha(&char_two)
                                                    .expect(format!("Char {} not found in alphabet", char_two as char)
                                                                .as_str());
                    char_three_index = self.get_index_in_alpha(&char_three)
                                                    .expect(format!("Char {} not found in alphabet", char_three as char)
                                                                .as_str());

                    decoded_bytes.push(char_one_index << 2 | char_two_index >> 4);
                    decoded_bytes.push(char_two_index << 4 | char_three_index >> 2);

                    index += 3;
                }
                2 => {
                    char_one = b64_data[index];
                    char_two = b64_data[index+1];

                    char_one_index = self.get_index_in_alpha(&char_one)
                                                    .expect(format!("Char {} not found in alphabet", char_one as char)
                                                                .as_str());
                    char_two_index = self.get_index_in_alpha(&char_two)
                                                    .expect(format!("Char {} not found in alphabet", char_two as char)
                                                                .as_str());

                    decoded_bytes.push(char_one_index << 2 | char_two_index >> 4);
                    
                    index += 2;
                }
                _ => {
                    return Err(());
                }
            }
        }

        Ok(decoded_bytes)
    }

    fn get_index_in_alpha(self: &Self, to_find: &u8) -> Option<u8> {
        // Return the index at which the byte is first found
        let mut index: u8 = 0;
        for b in self.alphabet.iter() {
            if b == to_find {
                return Some(index);
            }
            index += 1;
        }
        
        None
    }
}